# README
## Configuration

This bot requires a config file named `config.js`. This file must be placed in the root of the `src` folder. The file should look like this: 

```javascript
var config = {};

config.token = "BOT_TOKEN";
config.forecast = "FORECAST_API_KEY";

config.geocode = {};
config.geocode.provider = "GEOCODE_PROVIDER";
config.geocode.apiKey = "OPTIONAL_API_KEY";

config.superusers = [ 123456, 654321 ]; // Array of SU ids

config.help = 	"You can use the following commands:"
				+ "\r\n/help - Displays this text."
				+ "\r\n/stats - Displays chat stats."
				+ "\r\n/forecast [location] - Displays the weather at given location."
				+ "\r\n/echo [text] - Echos the text you sent."
				+ "\r\n/cool - Displays a random cool face."
				+ "\r\n/info - Displays information about this bot."
				+ "\r\n/doekoe - Displays when the money comes! (Alias: /moneyz)"
				+ "\r\n/leet [text] - Translates gives text into l33t text."
				+ "\r\n/8ball [yes/no question] - Answers your question."
				+ "\r\n/isup [hostname] - Checks if hostname is up."
				+ "\r\n/ishetaltijdvoorbier - Answers the very important question."
				+ "\r\n/joke - Tells a joke from /r/jokes."
				+ "\r\n/pokedex [ID/name] - Returns pokemon information.";

module.exports = config;
```

## Running the bot

If you just cloned or updated the bot run `npm install` from the `src` folder. To start to bot use `npm start`.

## Information for @BotFather

Command list:

```
help - Display all available commands.
stats - Displays chat stats.
forecast - Displays the weather at given location.
echo - Echos the text you sent.
cool - Displays a random cool face.
info - Displays information about this bot.
doekoe - Displays when the money comes! (Alias: /moneyz)
leet - Translates gives text into l33t text.
8ball - Answers your question.
isup - Checks if hostname is up.
ishetaltijdvoorbier - Answers the very important question.
joke - Tells a joke from /r/jokes.
pokedex - Returns pokemon information.
```

